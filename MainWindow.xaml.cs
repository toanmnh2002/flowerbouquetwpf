﻿using AppSettings;
using BusinessObject.Entities;
using DataAcess.IRepositories;
using NguyenManhToanWPF.CustomerUI;
using NguyenManhToanWPF.FlowerBouquetUI;
using NguyenManhToanWPF.OrderUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NguyenManhToanWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly Customer loginCustomer;
        private readonly ICustomerRepository _customerRepository;
        private readonly IFlowerBouquetRepository _flowerBouquetRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderDetailRepository _orderDetailRepository;

        public MainWindow(Customer loginCustomer, ICustomerRepository customerRepository, IFlowerBouquetRepository flowerBouquetRepository, IOrderRepository orderRepository, IOrderDetailRepository orderDetailRepository)
        {
            InitializeComponent();
            Title += $" | {Configuration.AppName}";
            this.loginCustomer = loginCustomer;
            _customerRepository = customerRepository;
            _flowerBouquetRepository = flowerBouquetRepository;
            _orderRepository = orderRepository;
            _orderDetailRepository = orderDetailRepository;
        }

        private void btnCustomerManagement_Click(object sender, RoutedEventArgs e)
        {
            var customerManagement=new CustomerManagement(loginCustomer,_customerRepository,_orderRepository,_orderDetailRepository,_flowerBouquetRepository);
            customerManagement.Closed += (s, args) => this.Close();
            this.Hide();
            customerManagement.Show();
        }

        private void btnFlowerBouquetManagement_Click(object sender, RoutedEventArgs e)
        {
            var flowerBouquetWindow = new FlowerBouquetWindow(loginCustomer, _customerRepository, _orderRepository, _orderDetailRepository, _flowerBouquetRepository);
            flowerBouquetWindow.Closed += (s, args) => this.Close();
            this.Hide();
            flowerBouquetWindow.Show();
        }

        private void btnOrderManagement_Click(object sender, RoutedEventArgs e)
        {
            var orderManagementWindow = new OrderManagement(loginCustomer, _customerRepository, _orderRepository, _orderDetailRepository, _flowerBouquetRepository);
            orderManagementWindow.Closed += (s, args) => this.Close();
            this.Hide();
            orderManagementWindow.Show();
        }
    }
}
