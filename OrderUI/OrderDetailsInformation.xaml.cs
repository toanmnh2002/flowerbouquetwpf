﻿using AppSettings;
using BusinessObject.Entities;
using DataAcess.IRepositories;
using System.Collections.Generic;
using System.Windows;

namespace NguyenManhToanWPF.OrderUI
{
    /// <summary>
    /// Interaction logic for OrderDetailsInformation.xaml
    /// </summary>
    public partial class OrderDetailsInformation : Window
    {
        private OrderDetail selectedOrderDetail;
        private int orderId;
        private PopupMode mode;
        private IOrderDetailRepository _orderDetailRepository;
        private IFlowerBouquetRepository _flowerBouquetRepository;
        private IEnumerable<FlowerBouquet> dataSource;

        public OrderDetailsInformation(int orderId, PopupMode mode, IOrderDetailRepository orderDetailRepository, IFlowerBouquetRepository flowerBouquetRepository, OrderDetail selectedOrderDetail = null)
        {
            InitializeComponent();
            Title += $" | {Configuration.AppName}";
            InitializeComponent();
            this.selectedOrderDetail = selectedOrderDetail;
            this.orderId = orderId;
            this.mode = mode;
            _orderDetailRepository = orderDetailRepository;
            _flowerBouquetRepository = flowerBouquetRepository;
        }


        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
