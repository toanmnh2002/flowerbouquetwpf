﻿using AppSettings;
using BusinessObject.Entities;
using DataAcess.IRepositories;
using DataAcess.Repositories;
using NguyenManhToanWPF.CustomerUI;
using NguyenManhToanWPF.FlowerBouquetUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NguyenManhToanWPF.OrderUI
{
    /// <summary>
    /// Interaction logic for OrderManagement.xaml
    /// </summary>
    public partial class OrderManagement : Window
    {
        private readonly Customer loginCustomer;
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderDetailRepository _orderDetailRepository;
        private readonly IFlowerBouquetRepository _flowerBouquetRepository;
        IEnumerable<Order> dataSource;
        public OrderManagement(Customer loginCustomer, ICustomerRepository customerRepository, IOrderRepository orderRepository, IOrderDetailRepository orderDetailRepository, IFlowerBouquetRepository flowerBouquetRepository)
        {
            InitializeComponent();
            Title += $" | {Configuration.AppName}";
            this.loginCustomer = loginCustomer;
            _customerRepository = customerRepository;
            _orderRepository = orderRepository;
            _orderDetailRepository = orderDetailRepository;
            _flowerBouquetRepository = flowerBouquetRepository;
            WindowLoad();
        }
        private void WindowLoad()
        {
            try
            {
                if (loginCustomer is null)
                {
                    throw new Exception("Login information is invalid! Please exit the application and try again...");
                }
                if (!loginCustomer.Email.Equals("admin@FUflowerbouquet.com"))
                {
                    btnAdd.Visibility = Visibility.Hidden;
                    btnDelete.Visibility = Visibility.Hidden;
                    btnExportData.Visibility = Visibility.Hidden;
                    topMenuAdmin.Visibility = Visibility.Hidden;
                    topMenuCustomer.Visibility = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Order Management", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void menuCustomer_Click(object sender, RoutedEventArgs e)
        {
            CustomerManagement customerManagement = new CustomerManagement(loginCustomer, _customerRepository, _orderRepository, _orderDetailRepository, _flowerBouquetRepository);
            customerManagement.Closed += (s, args) => this.Close();
            this.Hide();
            customerManagement.Show();
        }
        private void menuFlowerBouquet_Click(object sender, RoutedEventArgs e)
        {
            FlowerBouquetWindow flowerBouquetWindow = new FlowerBouquetWindow(loginCustomer, _customerRepository, _orderRepository, _orderDetailRepository, _flowerBouquetRepository);
            flowerBouquetWindow.Closed += (s, args) => this.Close();
            this.Hide();
            flowerBouquetWindow.Show();
        }
        private void Exit()
        {
            MessageBoxResult result = MessageBox.Show("Do you really want to exit?", "Customer Management",
                        MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result is MessageBoxResult.Yes)
            {
                Close();
            }
        }
        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            dataSource = new List<Order>();
            try
            {
                if (!loginCustomer.Email.Equals("admin@FUflowerbouquet.com"))
                {
                    dataSource = _orderRepository.GetAllByCustomerId(loginCustomer.CustomerId);
                }
                else
                {
                    dataSource = _orderRepository.GetAll();
                }
                ListOrder.ItemsSource = null;
                ListOrder.ItemsSource = dataSource;
                if (!loginCustomer.Email.Equals("admin@FUflowerbouquet.com")
                    && !dataSource.Any())
                {
                    MessageBox.Show("You don't have any order yet!",
                        "Load Order", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Order Management", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            var orderInformationDetails = new OrderManagementDetails(PopupMode.Add, loginCustomer, _customerRepository, _orderRepository, _orderDetailRepository, _flowerBouquetRepository
               );
            if (orderInformationDetails.ShowDialog() is not false)
            {
                LoadOrderList();
            }
        }

        private void LoadOrderList()
        {
            dataSource = new List<Order>();
            try
            {
                if (!loginCustomer.Email.Equals("admin@FUflowerbouquet.com"))
                {
                    dataSource = _orderRepository.GetAllByCustomerId(loginCustomer.CustomerId);
                }
                else
                {
                    dataSource = _orderRepository.GetAll();
                }
                ListOrder.ItemsSource = null;
                ListOrder.ItemsSource = dataSource;
                if (!loginCustomer.Email.Equals("admin@FUflowerbouquet.com")
                    && !dataSource.Any())
                {
                    MessageBox.Show("You don't have any order yet!",
                        "Load Order", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Order Management", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtOrderId.Text))
                {
                    throw new Exception("No order is selected!!");
                }
                MessageBoxResult result = MessageBox.Show($"Do you really want to delete the order with the ID {txtOrderId.Text}??",
                    "Delete Order", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result is MessageBoxResult.Yes)
                {
                    _orderRepository.Delete(int.Parse(txtOrderId.Text));
                    MessageBox.Show("Delete order successfully!!",
                        "Delete Order", MessageBoxButton.OK, MessageBoxImage.Information);
                    LoadOrderList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Delete order",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Order selectedOrder = ((ListViewItem)sender).Content as Order;
            var orderInformationWindow = new OrderManagementDetails(PopupMode.Update,loginCustomer,_customerRepository,_orderRepository,_orderDetailRepository,_flowerBouquetRepository,selectedOrder
                );
            if (orderInformationWindow.ShowDialog() is not false)
            {
                LoadOrderList();
            }
        }

        private void btnExportData_Click(object sender, RoutedEventArgs e)
        {
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
        }

        private void cboOrderStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        private void menuExit_Click(object sender, RoutedEventArgs e) => Exit();

        private void menuProfile_Click(object sender, RoutedEventArgs e)
        {
            var customerDetailsWindow = new CustomerDetails(PopupMode.Update, loginCustomer, _customerRepository, loginCustomer, _orderRepository, _orderDetailRepository, _flowerBouquetRepository
               );
            this.Hide();
            customerDetailsWindow.Closed += (s, args) => this.Close();
            customerDetailsWindow.Show();
        }
    }
}
