﻿using AppSettings;
using BusinessObject.Entities;
using DataAcess.IRepositories;
using DataAcess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Validation;

namespace NguyenManhToanWPF.OrderUI
{
    /// <summary>
    /// Interaction logic for OrderManagementDetails.xaml
    /// </summary>
    public partial class OrderManagementDetails : Window
    {
        private readonly PopupMode mode;
        private Order selectedOrder;
        private readonly Customer loginCustomer;
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderDetailRepository _orderDetailRepository;
        private readonly IFlowerBouquetRepository _flowerBouquetRepository;
        IEnumerable<Customer> customers;

        public OrderManagementDetails(PopupMode mode, Customer loginCustomer, ICustomerRepository customerRepository, IOrderRepository orderRepository, IOrderDetailRepository orderDetailRepository, IFlowerBouquetRepository flowerBouquetRepository, Order selectedOrder=null)
        {
            InitializeComponent();
            Title += $" | {Configuration.AppName}";
            this.mode = mode;
            this.selectedOrder = selectedOrder;
            this.loginCustomer = loginCustomer;
            _customerRepository = customerRepository;
            _orderRepository = orderRepository;
            _orderDetailRepository = orderDetailRepository;
            _flowerBouquetRepository = flowerBouquetRepository;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadCustomerList();
            if (mode is PopupMode.Add)
            {
                selectedOrder = new Order()
                {
                    OrderId = _orderRepository.GetNextId(),
                    OrderDate = DateTime.Now,
                    ShippedDate = DateTime.Now,
                    Total = 0,
                    OrderStatus = ""
                };
                btnAdd.Visibility = Visibility.Visible;
                btnUpdate.Visibility = Visibility.Hidden;
            }
            else if (mode is PopupMode.Update)
            {
                btnAdd.Visibility = Visibility.Hidden;
                btnUpdate.Visibility = Visibility.Visible;
            }
          LoadCustomerList();
        }
        private Order GetOrderObject() => new Order
        {
            OrderId = int.Parse(txtOrderId.Text),
            CustomerId = _customerRepository.GetByEmail(cboCustomer.SelectedItem.ToString()).CustomerId,
            OrderDate = txtOrderDate.SelectedDate.HasValue ? txtOrderDate.SelectedDate.Value : default(DateTime),
            ShippedDate = txtShippedDate.SelectedDate.HasValue?txtShippedDate.SelectedDate.Value:default(DateTime),
            Total = decimal.Parse(txtTotal.Text),
            OrderStatus = txtOrderStatus.Text
        };
        private void LoadCustomerList()
        {
            try
            {
                cboCustomer.ItemsSource = null;
                customers = _customerRepository.GetAll();
                if (customers is null || customers.Count() is 0)
                {
                    throw new Exception("There is no customer to load..." +
                        " Please try to add at least one customer to operate this action!");
                }
                List<string> customerEmaiList = new List<string>();
                foreach (var customer in customers)
                {
                    customerEmaiList.Add(customer.Email);
                }
                cboCustomer.ItemsSource = customerEmaiList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Loading customer list...",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void CheckObject()
        {
            if (string.IsNullOrEmpty(txtOrderId.Text) ||
                txtOrderId.Text.Equals("-1"))
            {
                throw new Exception("Order ID cannot be empty or it is invalid!!" +
                    " Please contact the developer for more information!");
            }
            if (cboCustomer.SelectedItem is null ||
                string.IsNullOrEmpty(cboCustomer.SelectedItem.ToString()))
            {
                throw new Exception("You have to choose a Customer for this order!!");
            }
            if (txtOrderDate.SelectedDate.HasValue == null)
            {
                throw new Exception("Order Date cannot be empty!!");
            }
            if (txtShippedDate.SelectedDate.HasValue == null)
            {
                throw new Exception("Shipped Date cannot be empty!!");
            }
            if (!NumberValidation.IsDecimal(txtTotal.Text) ||
                decimal.Parse(txtTotal.Text) <= 0)
            {
                throw new Exception("Total has to be a positive number!!");
            }
            if (string.IsNullOrEmpty(txtOrderStatus.Text))
            {
                throw new Exception("OrderStatus cannot be empty!");
            }
        }
        private void cboOrderStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        private void LoadOrder()
        {
        }
        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnEditOrderItem_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
