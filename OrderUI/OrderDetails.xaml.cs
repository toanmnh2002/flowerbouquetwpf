﻿using AppSettings;
using BusinessObject.Entities;
using DataAcess.IRepositories;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace NguyenManhToanWPF.OrderUI
{
    /// <summary>
    /// Interaction logic for OrderDetails.xaml
    /// </summary>
    public partial class OrderDetails : Window
    {
        private Customer loginCustomer;
        private int orderId;
        private IOrderDetailRepository _orderDetailRepository;
        private IFlowerBouquetRepository _flowerBouquetRepository;
        private IEnumerable<OrderDetail> dataSource;

        public OrderDetails(Customer loginCustomer, int orderId, IOrderDetailRepository orderDetailRepository, IFlowerBouquetRepository flowerBouquetRepository)
        {
            InitializeComponent();
            Title += $" | {Configuration.AppName}";
            this.loginCustomer = loginCustomer;
            this.orderId = orderId;
            _orderDetailRepository = orderDetailRepository;
            _flowerBouquetRepository = flowerBouquetRepository;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (loginCustomer is null)
                {
                    throw new Exception("Login information is invalid! Please exit the application and try again...");
                }

                LoadOrderDetails();

                if (!loginCustomer.Email.Equals("admin@FUflowerbouquet.com"))
                {
                    btnAdd.Visibility = Visibility.Hidden;
                    btnDelete.Visibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Order Details",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void LoadOrderDetails()
        {
            dataSource = new List<OrderDetail>();
            try
            {
                ListOrderDetails.ItemsSource = null;
                dataSource = _orderDetailRepository.GetAllByOrderId(orderId);
                ListOrderDetails.ItemsSource = dataSource;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Order Details",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            OrderDetailsInformation orderDetailsInformationWindow = new OrderDetailsInformation(
               orderId, PopupMode.Add, _orderDetailRepository, _flowerBouquetRepository);
            if (orderDetailsInformationWindow.ShowDialog() is not false)
            {
                LoadOrderDetails();
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtOrderId.Text))
                {
                    throw new Exception("No order detail is selected!!");
                }
                MessageBoxResult result = MessageBox.Show($"Do you really want to delete this order detail? \n" +
                    $"- Order ID: {orderId}\n" +
                    $"- FlowerBouquet: {txtFlowerBouquet.Text}", "Delete Order Details",
                    MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result is MessageBoxResult.Yes)
                {
                    _orderDetailRepository.Delete(int.Parse(txtOrderId.Text),
                        _flowerBouquetRepository.GetByName(txtFlowerBouquet.Text).FlowerBouquetId);
                    MessageBox.Show("Delete order detail successfully!!",
                        "Delete Order", MessageBoxButton.OK, MessageBoxImage.Information);
                    LoadOrderDetails();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Delete Order Details",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void listOrderDetails_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (loginCustomer.Email.Equals("admin@FUflowerbouquet.com"))
            {

                OrderDetail selectedOrderDetail = ((ListViewItem)sender).Content as OrderDetail;
                OrderDetailsInformation orderDetailsInformation = new OrderDetailsInformation(selectedOrderDetail.OrderId, PopupMode.Update, _orderDetailRepository, _flowerBouquetRepository, selectedOrderDetail);
                if (orderDetailsInformation.ShowDialog() is not false)
                {
                    LoadOrderDetails();
                }
            }
        }
    }
}
