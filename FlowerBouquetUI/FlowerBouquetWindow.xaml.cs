﻿using AppSettings;
using BusinessObject.Entities;
using DataAcess.IRepositories;
using DataAcess.Repositories;
using NguyenManhToanWPF.CustomerUI;
using NguyenManhToanWPF.OrderUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NguyenManhToanWPF.FlowerBouquetUI
{
    /// <summary>
    /// Interaction logic for FlowerBouquetWindow.xaml
    /// </summary>
    public partial class FlowerBouquetWindow : Window
    {
        private readonly Customer loginCustomer;
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderDetailRepository _orderDetailRepository;
        private readonly IFlowerBouquetRepository _flowerBouquetRepository;
        IEnumerable<FlowerBouquet> dataSource;
        public FlowerBouquetWindow(Customer loginCustomer, ICustomerRepository customerRepository, IOrderRepository orderRepository, IOrderDetailRepository orderDetailRepository, IFlowerBouquetRepository flowerBouquetRepository)
        {
            InitializeComponent();
            Title += $" | {Configuration.AppName}";
            this.loginCustomer = loginCustomer;
            _customerRepository = customerRepository;
            _orderRepository = orderRepository;
            _orderDetailRepository = orderDetailRepository;
            _flowerBouquetRepository = flowerBouquetRepository;
        }
        private void menuCustomer_Click(object sender, RoutedEventArgs e)
        {
            CustomerManagement customerManagement = new CustomerManagement(loginCustomer, _customerRepository, _orderRepository, _orderDetailRepository, _flowerBouquetRepository);
            customerManagement.Closed += (s, args) => this.Close();
            this.Hide();
            customerManagement.Show();
        }
        private void menuOrder_Click(object sender, RoutedEventArgs e)
        {
            OrderManagement orderManagement = new OrderManagement(loginCustomer,_customerRepository,_orderRepository,_orderDetailRepository,_flowerBouquetRepository);
            orderManagement.Closed += (s, args) => this.Close();
            this.Hide();
            orderManagement.Show();
        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string search = txtSearch.Text;
                if (string.IsNullOrEmpty(search))
                {
                    throw new Exception("Please enter search value...");
                }
                if (rdSearchId.IsChecked is true)
                {
                    if (!int.TryParse(search, out _))
                    {
                        throw new Exception("FloweBouquet ID has to be an integer!!");
                    }
                    int searchId = int.Parse(search);
                    IEnumerable<FlowerBouquet> flowerBouquets = _flowerBouquetRepository.SearchById(searchId);
                    if (flowerBouquets is null || flowerBouquets.Count() is 0)
                    {
                        MessageBox.Show("No result found!", "Search FlowerBouquet",
                            MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        ListFlowerBounquet.ItemsSource = flowerBouquets;
                    }
                }
                else if (rdSearchName.IsChecked is true)
                {
                    IEnumerable<FlowerBouquet> flowerBouquets = _flowerBouquetRepository.SearchByName(search);
                    if (flowerBouquets is null || !flowerBouquets.Any())
                    {
                        MessageBox.Show("No result found!", "Search Product",
                            MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        ListFlowerBounquet.ItemsSource = flowerBouquets;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Search FlowrBouquet",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            LoadFlowerBouquetList();
        }
        private void LoadFlowerBouquetList()
        {
            dataSource = new List<FlowerBouquet>();
            try
            {
                dataSource = _flowerBouquetRepository.GetAll();
                ListFlowerBounquet.ItemsSource = null;
                ListFlowerBounquet.ItemsSource = dataSource;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "FlowerBounquet Management", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            var flowerBouquetDetailsWindow = new FlowerBouquetDetails(PopupMode.Add,_flowerBouquetRepository);
            if (flowerBouquetDetailsWindow.ShowDialog() is not false)
            {
                LoadFlowerBouquetList();
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtFlowerBouquetId.Text))
                {
                    throw new Exception("No product is selected!!");
                }
                MessageBoxResult result = MessageBox.Show($"Do you really want to delete product with the ID {txtFlowerBouquetId.Text} ?", "Delete FlowerBouquet",
                    MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result is MessageBoxResult.Yes)
                {
                    _flowerBouquetRepository.Delete(int.Parse(txtFlowerBouquetId.Text));
                    MessageBox.Show("Delete FlowerBouquet successfully!!", "Delete FlowerBouquet", MessageBoxButton.OK,
                        MessageBoxImage.Information);
                    LoadFlowerBouquetList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Delete FlowerBouquet", MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        private void ListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            FlowerBouquet selectedFlowerBouquet = ((ListViewItem)sender).Content as FlowerBouquet;
            var flowerBouquettDetailsWindow = new FlowerBouquetDetails(PopupMode.Update, _flowerBouquetRepository, selectedFlowerBouquet);

            if (flowerBouquettDetailsWindow.ShowDialog() is not false)
            {
                LoadFlowerBouquetList();
            }
        }
        private void menuExit_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Do you really want to exit?", "FlowerBouquet Management",
                      MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result is MessageBoxResult.Yes)
            {
                Close();
            }
        }
    }
}
