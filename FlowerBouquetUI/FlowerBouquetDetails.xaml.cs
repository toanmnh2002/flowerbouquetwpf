﻿using AppSettings;
using BusinessObject.Entities;
using DataAcess.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NguyenManhToanWPF.FlowerBouquetUI
{
    /// <summary>
    /// Interaction logic for FlowerBouquetDetails.xaml
    /// </summary>
    public partial class FlowerBouquetDetails : Window
    {
        private readonly FlowerBouquet selectedFlowerBouquet;
        private readonly PopupMode mode;
        private readonly IFlowerBouquetRepository _flowerBouquetRepository;
        public FlowerBouquetDetails(PopupMode mode, IFlowerBouquetRepository flowerBouquetRepository, FlowerBouquet selectedFlowerBouquet = null)
        {
            InitializeComponent();
            Title += $" | {Configuration.AppName}";
            this.selectedFlowerBouquet = selectedFlowerBouquet;
            this.mode = mode;
            _flowerBouquetRepository = flowerBouquetRepository;
        }
        private FlowerBouquet GetProductObject() => new FlowerBouquet
        {
            FlowerBouquetId = int.Parse(txtFlowerBouquetId.Text),
            FlowerBouquetName = txtFlowerBouquetName.Text,
            CategoryId = int.Parse(txtCategory.Text),
            Description = txtDescription.Text,
            FlowerBouquetStatus = byte.Parse(txtFlowerBouquetStatus.Text),
            SupplierId = int.Parse(txtSupplier.Text),
            UnitPrice = decimal.Parse(txtUnitPrice.Text),
            UnitsInStock = int.Parse(txtUnitsInStock.Text)
        };
        private void CheckObject()
        {
            if (string.IsNullOrEmpty(txtFlowerBouquetId.Text) || txtFlowerBouquetId.Text.Equals("-1"))
            {
                throw new Exception("FlowerBouquet ID cannot be empty or it is invalid! ");
            }
            if (string.IsNullOrEmpty(txtFlowerBouquetName.Text))
            {
                throw new Exception("FlowerBouquet name cannot be empty!!");
            }
            if (string.IsNullOrEmpty(txtDescription.Text))
            {
                throw new Exception("Description cannot be empty!!");
            }
            if (!int.TryParse(txtCategory.Text, out _) || int.Parse(txtCategory.Text) <= 0)
            {
                throw new Exception("Category has to be a positive number!!");
            }
            if (!decimal.TryParse(txtUnitPrice.Text, out _) || decimal.Parse(txtUnitPrice.Text) <= 0)
            {
                throw new Exception("Unit Price has to be a positive number!!");
            }
            if (!int.TryParse(txtUnitsInStock.Text, out _) || int.Parse(txtUnitsInStock.Text) < 0)
            {
                throw new Exception("Units In Stock has to be a positive number!!");
            }
            if (!int.TryParse(txtSupplier.Text, out _) || int.Parse(txtSupplier.Text) < 0)
            {
                throw new Exception("SupplierId has to be a positive number!!");
            }
            if (!byte.TryParse(txtFlowerBouquetStatus.Text, out _) || int.Parse(txtFlowerBouquetStatus.Text) < 0)
            {
                throw new Exception("FlowerBouquetStatus is invalid!");
            }
        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Do you really want to exit?", "FlowerBouquetDetails",
                      MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result is MessageBoxResult.Yes)
            {
                Close();
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckObject();
                FlowerBouquet newFlowerBouquet = _flowerBouquetRepository.Add(GetProductObject());
                if (newFlowerBouquet is null)
                {
                    throw new Exception("An error has occured when adding a new flowerBouquet! Please try again...");
                }
                MessageBox.Show($"FlowerBouquet: {newFlowerBouquet.FlowerBouquetName} is added successfully!!", "Add new FlowerBouquet",
                    MessageBoxButton.OK, MessageBoxImage.Information);
                DialogResult = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Add new FlowerBouquet", MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckObject();
                FlowerBouquet updatedFlowerBouquet = _flowerBouquetRepository.Update(GetProductObject());
                if (updatedFlowerBouquet is null)
                {
                    throw new Exception("An error has occured when updating the FlowerBouquet! Please try again...");
                }
                MessageBox.Show($"FlowerBouquet {updatedFlowerBouquet.FlowerBouquetName} is updated successfully!", "Update FlowerBouquet",
                    MessageBoxButton.OK, MessageBoxImage.Information);
                DialogResult = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Update FlowerBouquet", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
