﻿using AppSettings;
using BusinessObject.Entities;
using DataAcess.IRepositories;
using DataAcess.Repositories;
using NguyenManhToanWPF.CustomerUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NguyenManhToanWPF
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderDetailRepository _orderDetailRepository;
        private readonly IFlowerBouquetRepository _flowerBouquetRepository;
        public LoginWindow(ICustomerRepository customerRepository, IOrderRepository orderRepository, IOrderDetailRepository orderDetailRepository, IFlowerBouquetRepository flowerBouquetRepository)
        {
            InitializeComponent();
            this.Title += $"{Configuration.AppName}";
            _customerRepository = customerRepository;
            _orderRepository = orderRepository;
            _orderDetailRepository = orderDetailRepository;
            _flowerBouquetRepository = flowerBouquetRepository;
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string email = txtEmail.Text;
                string password = txtPassword.Password;

                if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
                {
                    throw new Exception("Email and Password should not be empty!");
                }

                Customer customer = _customerRepository.Login(email, password);

                if (customer is null)
                {
                    throw new Exception("Login information is not correct. Please try again!");
                }
                else
                {
                    MessageBox.Show($"Login successfully! Welcome {customer.Email}...", "Login",
                        MessageBoxButton.OK, MessageBoxImage.Information);
                    if (customer.Email.Equals("admin@FUflowerbouquet.com"))
                    {
                        MainWindow mainWindow = new MainWindow(customer,
                            _customerRepository, _flowerBouquetRepository,
                            _orderRepository, _orderDetailRepository);
                        this.Hide();
                        mainWindow.Closed += (s, args) => this.Close();
                        mainWindow.Show();
                    }
                    else
                    {
                        CustomerDetails customerDetails = new CustomerDetails(PopupMode.Update,customer,_customerRepository,customer,_orderRepository,_orderDetailRepository,_flowerBouquetRepository);
                        this.Hide();
                        customerDetails.Closed += (s, args) => this.Close();
                        customerDetails.Show();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Login", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Do you want to exit?", "Login", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result is MessageBoxResult.Yes)
            {
                Close();
            }
        }
    }
}