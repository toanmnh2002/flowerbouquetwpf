﻿using AppSettings;
using BusinessObject.Entities;
using DataAcess.IRepositories;
using DataAcess.Repositories;
using NguyenManhToanWPF.FlowerBouquetUI;
using NguyenManhToanWPF.OrderUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NguyenManhToanWPF.CustomerUI
{
    /// <summary>
    /// Interaction logic for CustomerManagement.xaml
    /// </summary>
    public partial class CustomerManagement : Window
    {
        private readonly Customer loginCustomer;
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderDetailRepository _orderDetailRepository;
        private readonly IFlowerBouquetRepository _flowerBouquetRepository;
        IEnumerable<Customer> dataSource;
        public CustomerManagement(Customer loginCustomer, ICustomerRepository customerRepository, IOrderRepository orderRepository, IOrderDetailRepository orderDetailRepository, IFlowerBouquetRepository flowerBouquetRepository)
        {
            InitializeComponent();
            Title += $" | {Configuration.AppName}";
            this.loginCustomer = loginCustomer;
            _customerRepository = customerRepository;
            _orderRepository = orderRepository;
            _orderDetailRepository = orderDetailRepository;
            _flowerBouquetRepository = flowerBouquetRepository;
            WindowLoad();
        }
        private void WindowLoad()
        {
            try
            {
                if (loginCustomer is null)
                {
                    throw new Exception("Login information is invalid! Please exit the application and try again...");
                }
                lblStatus.Text = $"Welcome {loginCustomer.Email}";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Customer Management", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        
        private void menuFlowerBouquet_Click(object sender, RoutedEventArgs e)
        {
            FlowerBouquetWindow flowerBouquetWindow = new FlowerBouquetWindow(loginCustomer, _customerRepository, _orderRepository, _orderDetailRepository, _flowerBouquetRepository);
            flowerBouquetWindow.Closed += (s, args) => this.Close();
            this.Hide();
            flowerBouquetWindow.Show();
        }

        private void menuOrder_Click(object sender, RoutedEventArgs e)
        {
            var orderManagement = new OrderManagement(loginCustomer, _customerRepository, _orderRepository, _orderDetailRepository, _flowerBouquetRepository);
            orderManagement.Closed += (s, args) => this.Close();
            this.Hide();
            orderManagement.Show();
        }

        private void Exit()
        {
            MessageBoxResult result = MessageBox.Show("Do you really want to exit?", "Customer Management",
                        MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result is MessageBoxResult.Yes)
            {
                Close();
            }
        }
        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            LoadCustomerList();
        }
        private void LoadCustomerList()
        {
            dataSource = new List<Customer>();
            try
            {
                dataSource = _customerRepository.GetAll();
                ListCustomer.ItemsSource = null;
                ListCustomer.ItemsSource = dataSource;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Customer Management", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void listCustomer_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Customer selectedItem = ((ListViewItem)sender).Content as Customer;
            CustomerDetails customerDetails = new CustomerDetails(PopupMode.Update, loginCustomer, _customerRepository, selectedItem);

            if (customerDetails.ShowDialog() is not false)
            {
                LoadCustomerList();
            }
        }
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            var customerDetails = new CustomerDetails(PopupMode.Add, loginCustomer, _customerRepository);
            if (customerDetails.ShowDialog() is not false)
            {
                LoadCustomerList();
            }
        }
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtCustomerId.Text))
                {
                    throw new Exception("No Customer is selected!!");
                }
                MessageBoxResult result = MessageBox.Show($"Do you really want to delete Customer with the ID {txtCustomerId.Text} - Name:{txtCustomerName.Name} ?", "Delete Customer",
                                                    MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result is MessageBoxResult.Yes)
                {
                    _customerRepository.Delete(int.Parse(txtCustomerId.Text));
                    MessageBox.Show($"Delete Customer successfully!!", "Delete this customer", MessageBoxButton.OK, MessageBoxImage.Information);
                    LoadCustomerList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Delete this Customer", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void menuExit_Click(object sender, RoutedEventArgs e) => Exit();
    }
}
