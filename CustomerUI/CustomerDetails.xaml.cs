﻿using AppSettings;
using BusinessObject.Entities;
using DataAcess.IRepositories;
using NguyenManhToanWPF.OrderUI;
using System;
using System.Windows;
using Validation;

namespace NguyenManhToanWPF.CustomerUI
{
    /// <summary>
    /// Interaction logic for CustomerDetails.xaml
    /// </summary>
    public partial class CustomerDetails : Window
    {
        private readonly PopupMode mode;
        private Customer selectedCustomer;
        private readonly Customer loginCustomer;
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderDetailRepository _orderDetailRepository;
        private readonly IFlowerBouquetRepository _flowerBouquetRepository;
        public CustomerDetails(PopupMode mode, Customer loginCustomer, ICustomerRepository customerRepository, Customer selectedCustomer = null, IOrderRepository orderRepository = null, IOrderDetailRepository orderDetailRepository = null, IFlowerBouquetRepository flowerBouquetRepository = null)
        {
            InitializeComponent();
            Title += $" | {Configuration.AppName}";
            this.mode = mode;
            this.selectedCustomer = selectedCustomer;
            this.loginCustomer = loginCustomer;
            _customerRepository = customerRepository;
            _orderRepository = orderRepository;
            _orderDetailRepository = orderDetailRepository;
            _flowerBouquetRepository = flowerBouquetRepository;
        }
        private void Window_Load(object sender, RoutedEventArgs e)
        {
            if (mode is PopupMode.Add)
            {
                selectedCustomer = new Customer()
                {
                    CustomerId = _customerRepository.GetNextId(),
                    Email = "",
                    Password = "",
                    CustomerName = "",
                    City = "",
                    Country = "",
                    Birthday = DateTime.Now
                };
                btnAdd.Visibility = Visibility.Visible;
                btnUpdate.Visibility = Visibility.Hidden;
            }
            else if (mode is PopupMode.Update)
            {
                txtEmail.IsReadOnly = true;
                btnAdd.Visibility = Visibility.Hidden;
                btnUpdate.Visibility = Visibility.Visible;
            }
            txtCustomerId.Text = selectedCustomer.CustomerId.ToString();
            txtEmail.Text = selectedCustomer.Email;
            txtPassword.Password = selectedCustomer.Password;
            txtCustomerName.Text = selectedCustomer.CustomerName;
            txtCity.Text = selectedCustomer.City;
            txtCountry.Text = selectedCustomer.Country;
            txtBirthday.SelectedDate = selectedCustomer.Birthday;
            if (!loginCustomer.Email.Equals("admin@FUflowerbouquet.com"))
            {
                topMenuDock.Visibility = Visibility.Visible;
                btnUpdate.Content = "Update your profile";
                btnUpdate.Width = 120;
            }
            else
            {
                topMenuDock.Visibility = Visibility.Hidden;
            }
        }
        private Customer GetObject() => new Customer
        {
            CustomerId = int.Parse(txtCustomerId.Text),
            Email = txtEmail.Text.Trim(),
            Password = txtPassword.Password,
            CustomerName = txtCustomerName.Text.Trim(),
            Birthday = txtBirthday.SelectedDate.HasValue ? txtBirthday.SelectedDate.Value : default(DateTime),
            City = txtCity.Text.Trim(),
            Country = txtCountry.Text.Trim()
        };
        private void CheckObject()
        {
            if (string.IsNullOrEmpty(txtCustomerId.Text) || txtCustomerId.Text.Equals("-1"))
            {
                throw new Exception("Customer ID cannot be empty or it is invalid!");
            }
            if (!StringValidation.IsEmail(txtEmail.Text.Trim()))
            {
                throw new Exception($"The email '{txtEmail.Text}' seems not to be a valid email address!");
            }
            if (!StringValidation.CheckLength(txtPassword.Password, 6, StringValidation.StringMode.MinLength))
            {
                throw new Exception("Password has to be at least 6 characters!");
            }
            if (string.IsNullOrEmpty(txtCustomerName.Text))
            {
                throw new Exception("Customer name cannot be empty!");
            }
            if (string.IsNullOrEmpty(txtCity.Text))
            {
                throw new Exception("City cannot be empty!");
            }
            if (string.IsNullOrEmpty(txtCountry.Text))
            {
                throw new Exception("Country cannot be empty!");
            }
            if (txtBirthday.SelectedDate is null)
            {
                throw new Exception("Birthday cannot be empty!");
            }
            if (txtBirthday.SelectedDate < new DateTime(1900, 1, 1) || txtBirthday.SelectedDate > DateTime.Now)
            {
                throw new Exception("Invalid birthday range!");
            }
        }
        private void menuOrder_Click(object sender, RoutedEventArgs e)
        {
            OrderManagement orderManagement = new OrderManagement(loginCustomer, _customerRepository, _orderRepository, _orderDetailRepository, _flowerBouquetRepository);
            orderManagement.Closed += (s, args) => this.Close();
            this.Hide();
            orderManagement.Show();
        }

        private void menuExit_Click(object sender, RoutedEventArgs e)
        {
            Exit();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckObject();
                Customer newCustomer = _customerRepository.Add(GetObject());
                if (newCustomer is null)
                {
                    throw new Exception("An error has occured when adding a new Customer! Please try again...");
                }
                MessageBox.Show($"Customer {newCustomer.Email} is added successfully!", "Add new Customer", MessageBoxButton.OK, MessageBoxImage.Information);
                DialogResult = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Add new Customer", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckObject();
                Customer updatedCustomer = _customerRepository.Update(GetObject());
                if (updatedCustomer is null)
                {
                    throw new Exception("An error has occured when updating a new Customer! Please try again...");
                }
                if (!loginCustomer.Email.Equals("admin@FUflowerbouquet.com"))
                {
                    MessageBox.Show($"Your profile has been updated successfully!", "Update Customer",
                        MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show($"Customer {updatedCustomer.Email} is updated successfully!",
                        "Update Customer", MessageBoxButton.OK, MessageBoxImage.Information);
                    this.DialogResult = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Update Customer", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void Exit()
        {
            MessageBoxResult result = MessageBox.Show("Do you really want to exit?", "Update profile",
                           MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result is MessageBoxResult.Yes)
            {
                Close();
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (!loginCustomer.Email.Equals("admin@FUflowerbouquet.com"))
            {
                Exit();
            }
        }
    }
}
